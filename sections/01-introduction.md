# Introduction {#sec:introduction}

In this phase the geometry generator application should generate texture
coordinates and normals for each vertex. In the 3D engine we must activate we
must activate the lighting and texturing functionalities, as well as read and
apply the normals and texture coordinates from the model files.  The demo scene
for this phase is the animated solar system with texturing and lightning.

\newpage

