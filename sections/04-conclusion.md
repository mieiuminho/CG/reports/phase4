# Conclusion {#sec:conclusion}

We implemented all the requests functionalities for the current phase as well
as the whole assignment.  We also made two extra complex 3D scene and
implemented both a first and third person camera system. Our `.xml` files are
meaningful in the sense that you can build a scene using them.

Regarding the camera modes: we implemented both the third person camera motion
and the first person camera motion.

We weren't able to implement view frustum culling.

We accomplished the goals of this assignment in every phase.